class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    word_length = referee.pick_secret_word
    guesser.register_secret_length(word_length)
    @board = Array.new(word_length, nil)
  end

  def take_turn
    guess = guesser.guess(@board)
    matches = referee.check_guess(guess)
    guesser.handle_response(guess, matches)
    update(matches)
    print "Secret Word: #{board}\n"
  end

  def update(matches)
    matches.each do |idx|
      @board[idx] = referee.secret_word[idx]
    end
  end
end

class HumanPlayer
  attr_reader :secret_word

  def register_secret_length(word_length)
    print "The secret word length is #{word_length}\n"
  end

  def guess(board)
    print "Secret Word: #{board}\n"
    print 'Enter a guess: '
    $stdin.gets.chomp
  end

  def pick_secret_word
    puts 'Enter a secret word: '
    @secret_word = $stdin.gets.chomp
    secret_word.length
  end

  def check_guess(guess)
    puts "Guess is #{guess}"
    result = secret_word.chars.map.with_index do |ch, idx|
      idx if guess == ch
    end
    result.compact
  end

  def handle_response(ch, _matches)
    if referee.secret_word.include?(ch)
      puts 'Correct'
    else
      puts 'No Matches'
    end
  end
end

class ComputerPlayer
  attr_reader :dictionary, :secret_word, :candidate_words

  def initialize(dictionary = File.readlines('dictionary.txt'))
    @dictionary = dictionary.map(&:chomp)
  end

  def register_secret_length(length)
    @candidate_words = dictionary.select { |e| e.length == length }
  end

  def guess(board)
    letters = candidate_words.join
    letters.delete!(board.join)
    hash = {}
    letters.each_char do |ch|
      hash[ch] = letters.count(ch)
    end
    hash.key(hash.values.max)
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    secret_word.length
  end

  def check_guess(guess)
    result = secret_word.chars.map.with_index do |ch, idx|
      idx if guess == ch
    end
    result.compact
  end

  def handle_response(ch, matches)
    @candidate_words = candidate_words.select do |word|
      matches == indices(ch, word)
    end
  end

  def indices(ch, word)
    result = []
    word.chars.each_with_index do |letter, idx|
      result << idx if ch == letter
    end
    result
  end
end
